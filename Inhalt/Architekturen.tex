\section{Architekturen}
Dieses Kapitel behandelt Architekturen und Ansätze zur optimalen Gestaltung von mobilen Systemen.

\subsection{Anpassung}
Mobile Systeme müssen sich unbedingt den sich immer wieder ändernden Umgebungsparametern anpassen können. Die eingangs thematisierten Verbindungsschwierigkeiten können nur mit entsprechend angepassten Architekturen in den Griff bekommen werden.

\textbf{Es gibt grundsätzlich 2 Stellschrauben bei Anpassungen}

\begin{description}
\item[Variation der Verteilung von Aufgaben und Verantwortlichkeiten] Bei Verbindungsabbruch führt der Client autonom gewisse Aufaben durch, die normalerweise der Server für ihn erledigen würde.
\item[Variation der Datenqualität] Regulierung der Wiedergabetreue \index{Wiedergabetreue} (\textit{Fidelity}), d.h. Anpassung der Orginalgetreue (dpi, Auflösung).
\end{description}

Desweiteren kann man die Verantwortung der Anpassung entweder stärker auf das Betriebssystem oder aber die Anwendung legen.

\subsubsection{Anpassung Betriebssystem}
Die Anwendung selbst erfährt eine transparente Anpassung. Das Betriebssystem handelt automatisch und damit so gut wie möglich. 

\textbf{Vorteil}
\begin{itemize}
\item Anwendung muss nicht geändert werden
\end{itemize}

\textbf{Nachteile}
\begin{itemize}
\item Universallösung und deswegen im Einzelfall ungeeignet bzw. sogar kontraproduktiv
\item extern genutzte Dienste bleiben hier trotzdem unerreichbar
\end{itemize}

\subsubsection{Anpassung Anwendung}
Hier übernimmt die Anwendung selbst die Aufgabe der Absicherung gegenüber Verbindungsproblemen.

\textbf{Vorteil}
\begin{itemize}
\item Abdeckung aller Bedürfnisse
\end{itemize}

\textbf{Nachteile}
\begin{itemize}
\item Arrangement von Betriebsmitteln nicht zentral handhabbar
\item Apps müssen geändert werden
\end{itemize}


\subsection{Application Awareness}
Bedeutet, dass eine Anwendung über ihre eigenen Bedürfnisse Bescheid weiß und entsprechend handeln kann.

\begin{itemize}
\item Qualität der Betriebsmittel müssen bestimmt werden
\item Anwendungen müssen agil sein, also rasch reagieren
\item zentrale Stellen zur Betriebsmittelregelung (Zugriff und Vergabe)
\end{itemize}


\subsection{3+1 View Model \index{3+1 View Model}}

\grafig{./Inhalt/Medien/3+1_view_model}{0.7}{Das 3+1 View Model}{fuchss}{fig_3_1_view_model}

\begin{description}
\item[Module View] Strukturen, Klassen, Interfaces und Pakete also \textit{Umsetzung der Funktionalität}
\item[Allocation View] Bibliotheken, Konfigurationsmanagement und Deployment also \textit{Software und ihre Beziehung zur realen Welt}
\item[Component / Connector View] dynamische Abläufe, also Funktionen, Daten-/Kontrollfluss
\begin{itemize}
\itembf{Komponenten} Objekte, Prozesse und Threads also \textit{die Erbringung der Funktionalität}
\itembf{Konnektoren} Shared Memory, Sockets, JDBC und ESB also \textit{der Kontroll- und Datenfluss zwischen Komponenten}
\end{itemize}
\end{description}

\grafig{./Inhalt/Medien/adaptionsmechanismen}{0.8}{Adaptionsmechanismen}{fuchss}{fig:adaptmech}

\subsection{Adaptionsmechanismen}
Grundsätzlich können 2 Arten der Adaption \index{Adaption} unterschieden werden.

\textbf{Strukturadaption \index{Adaption!Struktur}}
\begin{itemize}
\itembf{statisch} Hinzufügen, Entfernen, Ändern von Komponenten und Konnektoren
\itembf{dynamisch} Auswahl und Platzierung von Komponenten und Konnektoren
\end{itemize}

\textbf{Parameteradaption \index{Adaption!Parameter}}
\begin{itemize}
\itembf{statisch - Änderung Daten}
\begin{itemize}
\item Anreichern
\item Transformieren (\textit{Format})
\item Ersetzen (\textit{verlustfrei durch Konvertierung})
\item Reduzieren (\textit{Filtern})
\end{itemize}
\itembf{dynamisch - Änderung Kommunikation} 
\begin{itemize}
\item Form der Übertragung (\textit{Protokolle, Priorisierung})
\item Zeitpunkt
\item Art des Konnektors (\textit{indirekt über Proxy/Agent für mehr Flexibilität})
\end{itemize}
\end{itemize}

\grafig{./Inhalt/Medien/adaptionsverfahren}{0.9}{Adaptionsverfahren}{fuchss}{fig:adaptionsverfahren}

\subsection{Caching \index{Caching} und Hoarding \index{Hoarding}}

\begin{description}
\item[Caching] Betrifft die Speicherung. Häufig verwendete Daten werden vorgehalten um einen performanteren Zugriff zu ermöglichen.
\item[Hoarding] Betrifft den Zugriff. Gemäß des Benutzerverhaltens werden bei verfügbarer Serververbindung etliche Daten geholt und gehortet. Dieser Vorgang geschieht also vorausschauend.
\end{description}

\subsection{Modelle}
Nachfolgend werden einige Modelle behandelt, die einen robusten Aufbau des mobilen Systems zum Ziel haben.

\subsubsection{Mobile Client/Server Modell \index{Modell!Client/Server}}
Das mobile Gerät ist in der Rolle des Clients und fordert beim Server Daten an. Sollte es zu einem Abbruch der Verbindung kommen, so ist der Client (z.B. Smartphone) auf sich alleine gestellt. Der Client muss dann den Server emulieren, damit Operationen sinnvoll ausgeführt werden können. Zum Einsatz kommen Caching- und Hoardingverfahren.

\grafig{./Inhalt/Medien/modell_client_server}{0.9}{Mobiles Client-Server Modell}{fuchss}{fig:movile_client_server}


\subsubsection{Client/Agent/Server Modell \index{Modell!Client/Agent/Server}}
Bei diesem Modell kommt ergänzend ein sogenannter Agent hinzu. Er befindet sich zwischen Client und Server, ist dem Server aber grundsätzlich näher. Gegenüber dem Client agiert der Agent als Proxy.

Als "`dienstspezifische Agenten"' bezeichnet man Agenten, die für einen konkreten Dienst zuständig sind. So kann es in einem entsprechenden Setup sein, dass ein Client mit mehreren Agenten kommuniziert.

\grafig{./Inhalt/Medien/modell_client_agent_server}{0.9}{Client-Agent-Server Modell}{fuchss}{fig:client_agent_server}

\textbf{Aufgaben des Agenten}
\begin{itemize}
\item Weiterleitung und Queuing von Nachrichten zur Maskierung von Verbindungsabbrüchen
\item ggf. Eingriff in die Nachrichten wie Kompression, Filterung, Paketieren
\item Client informieren (Dienstqualität, Tarif)
\end{itemize}

\textbf{Position des Agenten}
\begin{itemize}
\item Normalerweise am besten nahe an der Basisstation des Clients um diesen "`loyal"' bedienen zu können
\item Achtung: Agent muss ggf. mit Client die Stationen wechseln
\item Falls dienstspezifisch ggf. auch näher am Dienst positioniert
\item Eventuell: strategische Positionierung, falls Agent mit mehreren Clients interagieren muss
\end{itemize}

Der Positionswechsel eines Agenten ist aufwendig und mit Auswertungen und komplexen Algorithmen verbunden. Mechanismen ähnlich wie bei Funkzellenwechsel.

\advantage{
\item gleicht Verbindungen mit geringer Bandbreite aus (Kompression)
\item durch Aufteilung in Strecken Client-Agent und Agent-Server kann je Strecke ein anderes Protokoll zum Einsatz kommen
}

\disadvantage{
\item wenn Funkverbindung schlecht ist, steht auch Agent nicht zur Verfügung
\item Client muss auf Agent zugeschnitten sein
\item Agent kann Übertragung Agent-Client optimieren, auf Client-Agent hat er direkt keinen Einfluss
}

\textbf{Ziele des Modells}
\begin{itemize}
\item Anpassungen am Server zu vermeiden
\item Aspekte der Mobilität vor dem Server zu verbergen
\end{itemize}

Das Modell bietet sich immer dann an, wenn bereits der Server existiert und nun eine mobile Lösung entwickelt werden soll.

\subsubsection{Client/Intercept/Server Modell \index{Modell!Client/Intercept/Server}}
Dieses Modell erweitert das Client-Agent-Server Modell um einen weiteren Agent, der nahe des Clients untergebracht ist und für diesen als lokaler Proxy agiert. 

Dieser Umstand macht es möglich, dass Anpassungen der Daten z.B. hinsichtlich ihrer Qualität nun in beide Richtungen durchgeführt werden können. Sowohl zum Client hin als auch vom Client weg. Ein weiterer Vorteil ist, dass Berechnungen zwischen den Agenten aufgeteilt werden können.

\grafig{./Inhalt/Medien/modell_interceptor}{0.9}{Client-Interceptor-Server Modell}{fuchss}{fig:modell_interceptor}

\textbf{Ziele des Modells}
\begin{itemize}
\item Fakt der Mobilität sowohl vor Server als auch vor dem Client zu verbergen
\item Klare Zuständigkeitstrennung
\end{itemize}

So können sowohl bestehende Server- als auch bestehende Client-Implementierungen weiter verwendet werden.

\disadvantage{
\item Client schwergewichtig, benötigt ausreichend Power
\item Entwicklungsaufwand zweier Agents statt nur einem
}

\textbf{Beispiel}
\grafig{./Inhalt/Medien/adisoft}{0.9}{Beispiel eines Intercept Modells}{adisoft}{fig:modelle_intercept_bsp}

Unter \cite{adisoft} sind Informationen über das Produkt "`MOBILEmanager"' \index{MOBILEmanager}einzusehen. Ziel dieses Produktes ist es, bestehende Lösungen für den mobilen Einsatz aufzurüsten. Dabei geht es um die möglichst transparente und effiziente Bereitstellung von regulären Diensten.


\subsubsection{Peer-to-Peer Modell \index{Modell!Peer-to-Peer}}
Hier gibt es die scharfe Trennung zwischen Client und Server nicht. Jeder Teilnehmer kann sowohl Client als auch Server-Funktionalität annehmen und umsetzen. Ein Beispiel hierfür ist die kollaborative Dokumenten-Bearbeitung.

\disadvantage{
\item Endgeräte mussen sehr viel Leistung aufbringen um beiden Funktionalitäten gerecht zu werden
\item Powermanagement (Schlaf-Wach-Zyklen) behindert evtl. On-Demand-Funktionalität
}

Auch bei diesem Modell können Agents hinzugefügt werden. Diese werden mitunter dazu benutzt, einzelne Geräte bzw. Services "`aufzuwecken"'.

\grafig{./Inhalt/Medien/modell_peer}{0.9}{Peer-to-Peer Model}{fuchss}{fig:peer}


\subsubsection{Zusammenfassung}
\begin{itemize}
\item Agenten sollen Einschränkungen mobiler Verbindungen ausgleichen
\item Agenten können auf verschiedenen Schichten implementiert werden
\item Multiple Agenten auf verschiedenen Schichten zur Schaffung von Mobilitätsvoraussetzungen
\end{itemize}

\grafig{./Inhalt/Medien/modelle_fazit_bsp}{0.9}{Beispiele für Agenten}{fuchss}{fig:modelle_fazit}

\subsubsection{Pipes \index{Pipe} and Filters \index{Filter}}
Sind ein Entwurfsmuster für Systeme, die einfach adaptierbar sein müssen.

\begin{itemize}
\item Je Bearbeitungsschritt ein Filter
\item zwischen zwei Filtern je eine Pipe (Konnektor \index{Pipe!Konnektor})
\end{itemize}

Sowohl die Filter als auch die Pipes sind austauschbar und damit flexibel miteinander kombinierbar. \piref{fig:pipes_filters} zeigt, dass mitunter mehrere Filter/Pipes eine Komponente formen.

\grafig{./Inhalt/Medien/pipes_filters}{0.8}{Pipes-and-Filters Entwurfsmuster}{fuchss}{fig:pipes_filters}


\paragraph{Filter}
\begin{itemize}
\item Sie verändern die Daten durch Ergänzung, Verfeinerung oder Transformation
\item Sie sind Grundlage für die Handlungen eines Agenten
\item Mehrere Filter stellen mehrere, unabhängige Teilschritte einer solchen Handlung dar
\item Dadurch ergibt sich eine optimale Wiederverwendbarkeit der Filter
\item Ein Filter hängt nur von der Ausgabe des vorherigen ab
\end{itemize}

\paragraph{Pipes}
Pipes verbinden und synchronisieren Filter. Man kann bei den Pipes zwei Typen von Konnektoren unterscheiden.

\textbf{implizite}
\begin{itemize}
\item Methodenaufruf
\item RPC
\end{itemize}

\textbf{explizite}
\begin{itemize}
\item OS-Mittel Queues, Shared Memory
\item Sprachenspezifisch Java Message Service, ESB
\item individuelle Implementierungen
\end{itemize}

\grafig{./Inhalt/Medien/pipes2}{0.8}{Pipes als Verbinder zwischen Komponenten und Filtern}{fuchss}{fig:pipes_details}

\advantage{
\item Flexibilität durch Austauschbarkeit der Filter
\item Effizient da einzelne Filterkomponenten parallelisierbar
}

\disadvantage{
\item Gemeinsame Nutzung von Zustandsinformationen, da Zustand durchpropagiert wird
\item Fehlerkanal wird notwendig
}

\subsubsection{FIPA \index{FIPA}}
Hinter FIPA - Foundation for Intelligent Physical Agents verbirgt sich eine Spezifikation für die Agentenverwaltung. \piref{fig:fipa} zeigt das grundlegende Prinzip von FIPA. Die Foundation wurde bereits 1996 gegründet. Die letzten größeren und relevanten Entwicklungen zum FIPA-Standard fanden vorallem um 2004/2005 statt.

\grafig{./Inhalt/Medien/fipa}{0.8}{FIPA}{fuchss}{fig:fipa}

\begin{itemize}
\item ein Agent arbeitet in einer gekapselten Ausführungsumgebung
\item je Agent ein eindeutig identifizierbarer Besitzer
\item je Agent ggf. mehrere dedizierter Kommunikationsschnittstellen
\item je Agentenplattform ein AMS - Agenten Management System
\begin{itemize}
\item erzeugen und zerstören von Agenteninstanzen
\item Monitoring von Agenten
\item Migration von Agenten zwischen den Plattformen
\end{itemize}
\item Agent hat Lifecycle, AMS wirkt auf diesen ein \piref{fig:fipa_lifecycle}
\end{itemize}

\citeblock{Das FIPA-Prinzip erinnert ein wenig and die Verwaltung von virtuellen Maschinen durch Hypervisoren.}{eigene Anmerkung}

\grafig{./Inhalt/Medien/fipa_lifecycle}{0.9}{Lifecycle eines FIPA-Agenten}{fuchss}{fig:fipa_lifecycle}

\subsubsection{Broker Pattern \index{Broker Pattern}}
Für mobile und verteilte Systeme von Bedeutung, da es zwischen verschiedenen Dienstendpunkten vermittelt. Ein verteiltes System besteht in der Regel aus unabhängigen und flexiblen Komponenten, deren Zusammenspiel geregelt werden muss.

\grafig{./Inhalt/Medien/broker_text}{0.8}{Broker}{fuchss}{fig:broker_text}

\problem{
\item viele einzelne Komponenten bedeuten enormen Kommunikationsbedarf
\item Entstehung von Abhängigkeit
}

\goal{
\item Dem Entwickler soll verborgen werden, ob das System verteilt ist oder nicht
\item nutzt eine Anwendung ein Objekt, sollte ihr nur dessen Schnittstelle bekannt sein
}

\textbf{Anforderungen}
\begin{itemize}
\item Komponenten müssen auf Dienste anderer Komponenten zugreifen
\item Austausch von Komponenten zur Laufzeit
\item verbergen von System- und Implementierungsspezifika
\end{itemize}

\textbf{Lösung}
\begin{itemize}
\item Einführung des Brokers
\item Dienste werden am Broker angemeldet
\item über den Broker können Clients die entsprechenden Dienste anfordern
\end{itemize}

\grafig{./Inhalt/Medien/broker_uml}{0.9}{UML Klassendiagramm eines Brokers}{fuchss}{fig:broker_uml}

\textbf{Konzeption}
Der Entwurfsablauf lässt sich in 5 Einzelbereiche unterteilen.
\begin{enumerate}
\itembf{Objektmodell definieren}
\itembf{Client-Server Interface fixieren}
\itembf{Broker Interface ableiten}
\itembf{Proxies entwerfen}
\itembf{Broker-Middleware realisieren}
\end{enumerate}


\paragraph{Schritt 1 - Server meldet Dienst beim Broker an}
In diesem Schritt registriert ein dienstanbietender Server sein Angebot an den Broker. Dieser registriert ihn in seinem Verzeichnis.
\grafig{./Inhalt/Medien/broker_step1}{0.9}{Schritt 1}{fuchss}{fig:broker_step1}

\paragraph{Schritt 2 - Client schickt Anfrage}
In diesem Schritt geht die Anfrage eines Clients über einen bestimmten Dienst ein. Der Server vermittelt den Client an den entsprechenden bzw. geeigneten Server.
\grafig{./Inhalt/Medien/broker_step2}{0.9}{Schritt 2}{fuchss}{fig:broker_step2}

\paragraph{Schritt 3 - Broker kommunizieren untereinander}
Bis zum Auffinden des entsprechenden Servers können mitunter einige Zwischenstellen angesprochen werden.
\grafig{./Inhalt/Medien/broker_step3}{0.9}{Schritt 3}{fuchss}{fig:broker_step3}

