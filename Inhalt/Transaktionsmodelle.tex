\section{Transaktionsmodelle \index{Transaktionsmodelle}}
Transaktionen \index{Transaktion} sind im Mobile Computing unverzichtbar, da sie dabei helfen, potentielle Konflikte einfacher aufzulösen und damit zu bereinigen. Solche Situationen entstehen normalerweise, wenn isolierte Dateneinträge abgeändert wurden. Hier helfen Transaktionsmodelle dabei, die Konsistenz zu wahren. Konflikte entstehen in Zusammenhang mit Schreiboperationen.

\citeblock{%
Transaktionen schützen gemeinsam genutzte Ressourcen gegen den gleichzeitigen Zugriff durch verschiedene Prozesse.%
}{\cite{fuchss}}

\textbf{In der Konsequenz bedeutet das}
\begin{itemize}
\item Atomarität
\item Unter- bzw. Abbrechbarkeit von Änderungen, da Ursprung wiederherstellbar
\item benötigt wird eine geeignete Middleware
\end{itemize}

\subsection{ACID \index{ACID}}
\begin{description}
\item[Atomar] nicht teilbar
\item[Consistent] vorher und nachher gelten Invarianzbedingungen
\item[Isolated] keine gegenseitige Beeinflussung, Serialisierbarkeit
\item[Durable] Beständigkeit des Resultats
\end{description}

\subsection{Arten}
\begin{description}
\item[flache] Folge von Operationen 
\item[verschachtelte] haben Sub-Transaktionen, tlw. parallelisierbar \warning{(\textbf{CAVE:} Gilt ACID für die oberste Transaktion, so gilt Dauerhaftigkeit nicht mehr für die Sub-Transaktionen)}
\item[verteilte] flache Transaktion, arbeitet auf verteilten Datenbeständen
\end{description}

\subsection{Implementierung}
Es gibt verschiedene Ansätze, die Transaktion mit all ihren ACID-Anforderungen umzusetzen.

\subsubsection{Eigener Arbeitsbereich}
Betroffene Daten werden zuerst in den Arbeitsbereich kopiert. Bei Abbruch wird dieser Bereich gelöscht und andernfalls zurückkopiert.

\subsubsection{Writeahead \index{Writeahead}}
Strikte Protokollierung der Änderungen an den betroffenen Daten. Im Falle eines Abbruchs erfolgt ein Rollback.

\begin{itemize}
\item Wer?
\item Was?
\item Transaktion Erfolgreich?
\end{itemize}

\subsubsection{Nebenläufigkeit \index{Transaktionsmodelle!Nebenläufigkeit}}
Mehrere Transaktionen könnten sich gegenseitig in die Quere kommen. Deswegen müssen sie an zentraler Stelle verwaltet und gesteuert werden können. \piref{fig:transact_nebenl} zeigt die 3 involvierten Komponenten.

\grafig{./Inhalt/Medien/transaktionen_nebenl}{1.0}{Manager für Transaktionen}{fuchss}{fig:transact_nebenl}

\piref{fig:transact_konfl} zeigt, dass eine Hintereinanderausführung problemlos möglich ist. Eine verschachtelte Ausführung jedoch nicht.

\grafig{./Inhalt/Medien/transaktionen_konflikte}{0.8}{Konflikte durch Schreiboperationen}{fuchss}{fig:transact_konfl}

\subsubsection{Optimistisch oder pessimistisch}
Es gibt auch hier zwei Ansätze, mögliche Konflikte aufzulösen.

\begin{description}
\item[optimistischer Ansatz] hier werden Operationen zunächst ausgeführt und dann später synchronisiert
\item[pessimistischer Ansatz] hier wird noch vor der Ausführung eine Synchronisation vorgenommen
\end{description} 

Der optimistische Ansatz ist der bevorzugte, da er vermeidet, dass viele Sperrungen vorgenommen werden müssen.

\subsection{Optimistische Nebenläufigkeitskontrolle}
\begin{itemize}
\item Daten können immer gelesen werden (Annahme, dass Lesen eben nichts verändert)
\item je Schreibaktion 3 Teile
\begin{enumerate}
\itembf{Lesephase}
\itembf{Validierungsphase} um sicherzustellen, dass Änderungen nicht zu Integritätsverlust führen
\itembf{Schreibphase} um lokale Änderungen global zu fixieren
\end{enumerate}
\item Falls Fehlschlag: Verwerfen der Transaktion und Löschen der lokal gespeicherten Daten \textbf{und} automatische Wiederholung der Transaktion
\end{itemize}

\citeblock{%
Ein solcher Fehlschlag tritt immer dann auf, wenn Daten, die innerhalb der Transaktion benötigt wurden, vor dem Abschließen der Transaktion bereits durch andere geändert wurden. \textit{(Was mit dem pessimistischen Ansatz nicht passiert wäre)}
}{\cite{fuchss}}

\warning{\textbf{Unauflösbare Konflikte müssen in letzter Instanz manuell gelöst werden!}}

\subsection{Aufbau von Transaktionen \index{Transaktionsmodelle!Aufbau}}
Transaktionen lassen sich gedanklich in drei Phasen einteilen. 

\begin{itemize}
\item lesen
\item bearbeiten
\item schreiben
\end{itemize}

Das Bearbeiten hat hier eine Sonderrolle. Die dabei entstehenden Daten sind lokal und betreffen unmittelbar keine anderen Objekte oder Interessen. Deswegen lässt sich sagen, dass eine Unterteilung in 2 Phasen ausreichend ist. \piref{fig:trans_phases} zeigt mögliche Kombinationen dieser 2 Phasen.

\begin{itemize}
\item lesen
\item schreiben
\end{itemize}

\grafig{./Inhalt/Medien/transact_phasen}{0.8}{Mögliche Zusammenhänge zwischen Schreib- und Lesephase}{fuchss}{fig:trans_phases}

Dadurch ergeben sich viele theoretische Kombinationen wie in \piref{fig:trans_moegl} zu sehen.

\grafig{./Inhalt/Medien/transact_6}{0.8}{Mögliche Phasenkombinationen}{fuchss}{fig:trans_moegl}

Da Lese- und Schreibphase immer im Verbund auftreten reduzieren sich die praktischen Möglichkeiten deutlich. \piref{fig:trans_uebrig} zeigt die dadurch übrig bleibenden Kombinationsmöglichkeiten, wenn sich Schreibphasen mit Lesephasen überdecken. \piref{PICTURE} zeigt die Möglichkeiten für sich überdeckende Lesephasen. \piref{fig:coll2} zeigt sich schneidende Lesephasen.

\grafig{./Inhalt/Medien/transact_pract}{0.8}{Praktisch relevante Kombinationen von Transaktionen, bei denen sich die Schreib- und Lesephasen überdecken}{fuchss}{fig:trans_uebrig}


\grafig{./Inhalt/Medien/transact_coll}{0.8}{Kombinationen von sich verdeckenden Lesephasen}{fuchss}{fig:coll}

\grafig{./Inhalt/Medien/transact_read_cross}{0.8}{Kombinationen von sich schneidenden Lesephasen}{fuchss}{fig:coll2}


\subsubsection{Absicherung \index{Transaktionsmodelle!Absicherung}}
Schrittweise kann die Absicherung verfeinert werden.

In einem ersten Schritt (\piref{fig:trans_v1}) wird die Schreibphase synchronisiert.

\grafig{./Inhalt/Medien/trans_v1}{0.8}{Schritt 1}{fuchss}{fig:trans_v1}

Im zweiten Schritt (\piref{fig:trans_v2}) wird eine zusätzliche Validierungsphase geschaffen.

\grafig{./Inhalt/Medien/trans_v2}{0.8}{}{Schritt 2}{fig:trans_v2}

Im dritten und letzten Schritt wird wie in \piref{fig:trans_v3} zu sehen die Validierungsphase zweigeteilt: In einen Lese- und einen Schreibteil. Hiervon ist der Schreibteil synchronisiert.

\grafig{./Inhalt/Medien/trans_v3}{0.8}{}{Schritt 3}{fig:trans_v3}


\subsubsection{Umsetzung \index{Transaktionsmodelle!Umsetzung}}
Zur Umsetzung der zuvor besprochenen Absicherung müssen Operationen eingeführt werden, um von Eingriffen in das Dateisystem zu abstrahieren.

\begin{description}
\item[read] Eingabeparameter der Transaktion, bei externer Änderung am gelesenen Datum kommt es zum Abbruch
\grafig{./Inhalt/Medien/timpl_read}{0.85}{Read}{fuchss}{fig:trimpl_read}
\item[write] Schreiben eines Datums ohne notwendiges vorheriges Lesen
\grafig{./Inhalt/Medien/timpl_write}{0.85}{Write}{fuchss}{fig:trimpl_write}
\item[create] Erzeugung eines Datums, soll verhindern, dass Datum schon besteht
\grafig{./Inhalt/Medien/timpl_create}{0.85}{Create}{fuchss}{fig:trimpl_create}
\item[delete] löscht Datum ohne notwendiges vorheriges Lesen
\grafig{./Inhalt/Medien/timpl_delete}{0.85}{Delete}{fuchss}{fig:trimpl_delete}
\end{description}


\subsubsection{Die Linearisierung $\pi$ \index{Linearisierung}}
Eine Menge nebenläufiger Transaktionen überführt einen Datenbestand von d\textsubscript{init} nach d\textsubscript{final}. Die Linearisierung $\pi$ beschreibt den Umstand, dass die hintereinander ausgeführten Transaktionen einen Anfangszustand d\textsubscript{init} nach d\textsubscript{final} transformieren. 

\textbf{Um das zu erreichen ist sicherzustellen}
\begin{itemize}
\item T\textsubscript{i} hat alle Schreiboperationen abgeschlossen, bevor T\textsubscript{j} mit Leseoperation beginnt
\item zu scheibende Daten von T\textsubscript{i} disjunkt zu denen die T\textsubscript{j} liest \textbf{sowie} die Schreibphase von T\textsubscript{i} beendet ist ehe die von T\textsubscript{j} beginnt
\item zu schreibende Daten von T\textsubscript{i} disjunkt zu denen die T\textsubscript{j} liegt oder schreibt \textbf{und} T\textsubscript{i} Lesephase abschließt ehe T\textsubscript{j} seine Lesephase abschließt
\end{itemize}

\citeblock{%
Mit anderen Worten, T\textsubscript{i} ist abgeschlossen bevor T\textsubscript{j} beginnt oder die Daten die T\textsubscript{i} schreibt sind für T\textsubscript{j} irrelevant und T\textsubscript{i} überschreibt keine Daten von T\textsubscript{j}, bzw. T\textsubscript{i} kann die von T\textsubscript{j} gar nicht überschreiben. In diesem Fall muss aber sichergestellt sein, dass die Daten die T\textsubscript{j} schreibt nicht von T\textsubscript{i} gelesen werden.%
}{\cite{fuchss}}

In \piref{fig:trans_big_picture} ist zur Verdeutlichung abschließend ein "`Big Picture"' zu sehen.

\grafig{./Inhalt/Medien/trans_lin_big_picture}{1.0}{Big Picture}{fuchss}{fig:trans_big_picture}






