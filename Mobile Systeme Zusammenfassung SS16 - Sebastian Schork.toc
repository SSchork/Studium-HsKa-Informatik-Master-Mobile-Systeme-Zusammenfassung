\select@language {ngerman}
\contentsline {section}{\numberline {I}Abbildungsverzeichnis}{I}{section.I}
\contentsline {section}{\numberline {II}Tabellenverzeichnis}{IV}{section.II}
\contentsline {section}{V Abk\IeC {\"u}rzungsverzeichnis}{1}{section.II}
\contentsline {section}{\numberline {1}Mobilit\IeC {\"a}t}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Mobile Computing}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Native App }{1}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Web-App }{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Hybrid-App }{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Vergleich}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Augmented Reality }{5}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Limitierende Faktoren}{6}{subsection.1.7}
\contentsline {subsubsection}{\numberline {1.7.1}Konnektivit\IeC {\"a}t}{6}{subsubsection.1.7.1}
\contentsline {subsubsection}{\numberline {1.7.2}Leistung}{6}{subsubsection.1.7.2}
\contentsline {subsubsection}{\numberline {1.7.3}Vertrauensw\IeC {\"u}rdigkeit}{7}{subsubsection.1.7.3}
\contentsline {subsubsection}{\numberline {1.7.4}Abw\IeC {\"a}rtskompatibilit\IeC {\"a}t}{7}{subsubsection.1.7.4}
\contentsline {section}{\numberline {2}Architekturen}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}Anpassung}{8}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Anpassung Betriebssystem}{8}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Anpassung Anwendung}{8}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Application Awareness}{9}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}3+1 View Model }{9}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Adaptionsmechanismen}{10}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Caching und Hoarding }{11}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Modelle}{11}{subsection.2.6}
\contentsline {subsubsection}{\numberline {2.6.1}Mobile Client/Server Modell }{11}{subsubsection.2.6.1}
\contentsline {subsubsection}{\numberline {2.6.2}Client/Agent/Server Modell }{12}{subsubsection.2.6.2}
\contentsline {subsubsection}{\numberline {2.6.3}Client/Intercept/Server Modell }{13}{subsubsection.2.6.3}
\contentsline {subsubsection}{\numberline {2.6.4}Peer-to-Peer Modell }{15}{subsubsection.2.6.4}
\contentsline {subsubsection}{\numberline {2.6.5}Zusammenfassung}{15}{subsubsection.2.6.5}
\contentsline {subsubsection}{\numberline {2.6.6}Pipes and Filters }{16}{subsubsection.2.6.6}
\contentsline {paragraph}{\numberline {2.6.6.1}Filter}{16}{paragraph.2.6.6.1}
\contentsline {paragraph}{\numberline {2.6.6.2}Pipes}{17}{paragraph.2.6.6.2}
\contentsline {subsubsection}{\numberline {2.6.7}FIPA }{17}{subsubsection.2.6.7}
\contentsline {subsubsection}{\numberline {2.6.8}Broker Pattern }{19}{subsubsection.2.6.8}
\contentsline {paragraph}{\numberline {2.6.8.1}Schritt 1 - Server meldet Dienst beim Broker an}{21}{paragraph.2.6.8.1}
\contentsline {paragraph}{\numberline {2.6.8.2}Schritt 2 - Client schickt Anfrage}{21}{paragraph.2.6.8.2}
\contentsline {paragraph}{\numberline {2.6.8.3}Schritt 3 - Broker kommunizieren untereinander}{22}{paragraph.2.6.8.3}
\contentsline {section}{\numberline {3}Datenmanagement}{23}{section.3}
\contentsline {subsection}{\numberline {3.1}Hoarding}{23}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Verfahren}{24}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}LRU - least recently used}{24}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}LOU - least often used}{24}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Prefetching}{24}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Cache}{25}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Positionierung}{28}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Architektur}{29}{subsubsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3}Emulation}{29}{subsubsection.3.4.3}
\contentsline {paragraph}{\numberline {3.4.3.1}Verfahren zur Optimierung von Speicherplatz bzw. Gr\IeC {\"o}\IeC {\ss }e}{30}{paragraph.3.4.3.1}
\contentsline {paragraph}{\numberline {3.4.3.2}Reintegration }{30}{paragraph.3.4.3.2}
\contentsline {subsection}{\numberline {3.5}Validierung}{30}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Zustandslose Validierung }{31}{subsection.3.6}
\contentsline {subsubsection}{\numberline {3.6.1}Broadcasting Timestamps}{31}{subsubsection.3.6.1}
\contentsline {paragraph}{\numberline {3.6.1.1}Optimierung bzgl. langer Disconnects}{32}{paragraph.3.6.1.1}
\contentsline {paragraph}{\numberline {3.6.1.2}Optimierung bzgl. Latenzzeit}{32}{paragraph.3.6.1.2}
\contentsline {paragraph}{\numberline {3.6.1.3}Optimierung bzgl. Bandbreite}{33}{paragraph.3.6.1.3}
\contentsline {subsection}{\numberline {3.7}Zustandsbasierte Validierung }{33}{subsection.3.7}
\contentsline {subsubsection}{\numberline {3.7.1}Callback Breaks }{33}{subsubsection.3.7.1}
\contentsline {subsubsection}{\numberline {3.7.2}Observer Pattern }{34}{subsubsection.3.7.2}
\contentsline {subsection}{\numberline {3.8}Fazit zu Cache-Updates }{35}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Weak Connectivity }{35}{subsection.3.9}
\contentsline {section}{\numberline {4}Transaktionsmodelle }{37}{section.4}
\contentsline {subsection}{\numberline {4.1}ACID }{37}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Arten}{37}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Implementierung}{37}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Eigener Arbeitsbereich}{38}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Writeahead }{38}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Nebenl\IeC {\"a}ufigkeit }{38}{subsubsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.4}Optimistisch oder pessimistisch}{39}{subsubsection.4.3.4}
\contentsline {subsection}{\numberline {4.4}Optimistische Nebenl\IeC {\"a}ufigkeitskontrolle}{39}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Aufbau von Transaktionen }{39}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Absicherung }{42}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Umsetzung }{43}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Die Linearisierung $\pi $ }{44}{subsubsection.4.5.3}
\contentsline {section}{\numberline {5}Exkurs - Praktische Umsetzung Adaptionsmechanismen }{46}{section.5}
\contentsline {subsection}{\numberline {5.1}\IeC {\"U}berlegung}{46}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Idee}{46}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Implementierung}{48}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Schritt 1 - Die ReST-Schnittstelle }{48}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Schritt 2 - Optimierung }{49}{subsubsection.5.3.2}
\contentsline {paragraph}{\numberline {5.3.2.1}Schritt 2.1 - Cache (Server)}{50}{paragraph.5.3.2.1}
\contentsline {paragraph}{\numberline {5.3.2.2}Schritt 2.2 - Cache (Client)}{50}{paragraph.5.3.2.2}
\contentsline {paragraph}{\numberline {5.3.2.3}Schritt 2.3 - Optimierung}{51}{paragraph.5.3.2.3}
\contentsline {subsection}{\numberline {5.4}Endergebnis - Big Picture}{51}{subsection.5.4}
\contentsline {section}{\numberline {6}Signal\IeC {\"u}bertragung}{53}{section.6}
\contentsline {subsection}{\numberline {6.1}Referenzmodell }{53}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Signaltypen}{54}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Analoge Modulationsverfahren }{54}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Amplitudenmodulation}{55}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Frequenz- und Phasenmodulation}{55}{subsubsection.6.3.2}
\contentsline {subsection}{\numberline {6.4}Digitale Modulationsverfahren }{56}{subsection.6.4}
\contentsline {subsubsection}{\numberline {6.4.1}QPSK }{56}{subsubsection.6.4.1}
\contentsline {subsubsection}{\numberline {6.4.2}QAM }{57}{subsubsection.6.4.2}
\contentsline {section}{\numberline {7}Multiplexverfahren }{58}{section.7}
\contentsline {subsection}{\numberline {7.1}Signalausbreitung }{58}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Zugriffsproblematik }{59}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}SDMA }{59}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}TDMA }{60}{subsection.7.4}
\contentsline {subsubsection}{\numberline {7.4.1}ALOHA }{61}{subsubsection.7.4.1}
\contentsline {subsection}{\numberline {7.5}FDMA }{62}{subsection.7.5}
\contentsline {subsection}{\numberline {7.6}OFDMA }{63}{subsection.7.6}
\contentsline {subsection}{\numberline {7.7}FHSS }{63}{subsection.7.7}
\contentsline {subsection}{\numberline {7.8}CDMA }{64}{subsection.7.8}
\contentsline {subsubsection}{\numberline {7.8.1}DSSS - Direct Sequence Spread Spectrum }{64}{subsubsection.7.8.1}
\contentsline {paragraph}{\numberline {7.8.1.1}Berechnung}{65}{paragraph.7.8.1.1}
\contentsline {paragraph}{\numberline {7.8.1.2}Beispiel}{65}{paragraph.7.8.1.2}
\contentsline {subsubsection}{\numberline {7.8.2}OVSF Spreizcodegenerierung }{66}{subsubsection.7.8.2}
\contentsline {subsubsection}{\numberline {7.8.3}OVSF Spreizfaktor}{67}{subsubsection.7.8.3}
\contentsline {section}{\numberline {8}Mobilfunknetze}{68}{section.8}
\contentsline {subsection}{\numberline {8.1}GSM }{68}{subsection.8.1}
\contentsline {subsubsection}{\numberline {8.1.1}Fakten}{68}{subsubsection.8.1.1}
\contentsline {subsubsection}{\numberline {8.1.2}Weiterentwicklungen}{68}{subsubsection.8.1.2}
\contentsline {subsubsection}{\numberline {8.1.3}Innovationen }{68}{subsubsection.8.1.3}
\contentsline {subsubsection}{\numberline {8.1.4}Zusammenh\IeC {\"a}nge }{69}{subsubsection.8.1.4}
\contentsline {subsubsection}{\numberline {8.1.5}Funkverkehr }{70}{subsubsection.8.1.5}
\contentsline {subsubsection}{\numberline {8.1.6}Frequenzplanung }{71}{subsubsection.8.1.6}
\contentsline {subsubsection}{\numberline {8.1.7}GPRS - Erweiterung f\IeC {\"u}r mobiles Internet }{72}{subsubsection.8.1.7}
\contentsline {subsection}{\numberline {8.2}UMTS }{73}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}Big Picture - Integration Infrastruktur}{75}{subsubsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.2}Betriebsmodi }{75}{subsubsection.8.2.2}
\contentsline {paragraph}{\numberline {8.2.2.1}UTRA/FDD - Frequency Division Duplex}{75}{paragraph.8.2.2.1}
\contentsline {paragraph}{\numberline {8.2.2.2}UTRA/TDD - Time Division Duplex}{76}{paragraph.8.2.2.2}
\contentsline {subsubsection}{\numberline {8.2.3}Macro Diversity }{77}{subsubsection.8.2.3}
\contentsline {subsubsection}{\numberline {8.2.4}Zellatmung GSM vs. UMTS }{78}{subsubsection.8.2.4}
\contentsline {subsection}{\numberline {8.3}LTE }{78}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Leistungsvergleich Mobilfunktechnologien }{79}{subsection.8.4}
\contentsline {section}{Literaturverzeichnis}{80}{section*.2}
\contentsline {section}{Index}{81}{section*.3}
